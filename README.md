# Fichiers ressources pour le collectif intim'idées


|- gfx : Design, charte graphique, illustrations...
|  |- logo : export png, format vectoriel svg, ai
|  |- font-police : polices de caractères
|  |- banniere : format pour réseaux sociaux, bannières pour évènement...
|- ressource-externe :
|  |- tramme-animation
|  |  |- atelier
|  |  |- cercle
|  |- guide-inforgraphie
|  |  |- livret
|  |  |- poster
|  |- autre
|  |  |- Holacratie
|  |  |- Félinades


